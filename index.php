<?php
/**
 * Use this template for News.
 */
get_header();
?>

<section class="section--formatted">
<?php
	if(have_posts()) {
		while(have_posts()) {
			the_post();
?>
			<div class="row">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php edit_post_link('Redigera sektion', '<span class="edit-link">', '</span>', get_the_ID()); ?>
			</div>
<?php
		}
	}
?>
</section>
<?php
get_footer();
