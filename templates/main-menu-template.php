<nav class="row main-menu--center">
	<input id="main-menu-button" class="main-menu-button" type="checkbox">
	<label for="main-menu-button">
		<span></span>
	</label>
<?php
	$args = array(
		'walker' => new hermit_walker(),
		'container' => false,
	);
	wp_nav_menu($args);
?>
</nav>
