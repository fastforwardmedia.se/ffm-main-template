<?php
/**
 * Template Name: Case Template
 *
 * @package WordPress
 * @subpackage ffm-main-template
 * @since FFM Main Template 1.1
 */
get_header();

if(have_posts()) {
?>
	<section class="section">
<?php
		while(have_posts()) {
			the_post();
?>
			<div class="row">
				<h1><?php the_title(); ?></h1>
				<p><?php the_content(); ?></p>
				<?php edit_post_link('Redigera sektion', '<span class="edit-link">', '</span>', get_the_ID()); ?>
			</div>
<?php
		}
?>
	</section>
<?php
}
get_footer();
