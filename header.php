<!doctype html>
<html>
    <head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title('-', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
    </head>
    <body <?php body_class();?>>
		<?php wp_after_body(); ?>
		<header class="header" role="navigation">
<?php
			if (get_theme_mod('themeslug_logo' )) {
?>
				<a class="page-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<img src="<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				</a>
<?php
			}
?>
			<?php get_template_part('templates/main-menu-template'); ?>
		</header>
<?php
	// In need of site hero use this:
	// echo header_image();
