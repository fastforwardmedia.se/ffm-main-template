<?php get_header(); ?>
<section class="section">
	<div class="row">
		<div class="col-12">
			<h1 class="margin-bottom--l">section.section</h1>
			<h2>Header h2</h2>
			<h3>Header h3</h3>
			<h4>Header h4</h4>
			<h5>Header h5</h5>
			<h6>Header h6</h6>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Paragraphs with quote, strong and em tags.</h2>
		<div class="col-12--small col-6--medium">
			<p>This is content in the <q>formatted wysiwyg section</q> (.section--formatted). It overwrites some of the basic styles, mainly lists. This part is a paragraph with a <strong>strong tag</strong> and some <em>italic content</em>.</p>

			<p>Here is another paragraph inside .formatted--section with another <q>cite</q> and here is a link to <a href="https:google.com" title="Google" alt="Google.com">Google</a>.
				Another cool thing is that we can wrap a word or more text into span with the class <span class="silent">.silent</span> or <span class="loud">.loud</span> to give them some extra visual meening.
			</p>
		</div>
		<div class="col-12--small col-6--medium">
			<blockquote>This is a blockquote</blockquote>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Notifications</h2>
		<div class="col-12--small col-3--medium">
			<p><span class="error">.error gives this color.</span></p>
			<p><span class="warning">.warning gives this color.</span></p>
			<p><span class="success">.success gives this color.</span></p>
		</div>
		<div class="col-12--small col-9--medium">
			<span class="notification-block--error">.notification-block--error</span>
			&nbsp;
			<span class="notification-block--warning">.notification-block--warning</span>
			&nbsp;
			<span class="notification-block--success">.notification-block--success</span>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Buttons</h2>
		<div class="col-12--small col-4--medium">
			<p><button class="button">.button</button></p>
			&nbsp;
			<p><button class="button--blank">.button--blank</button></p>
			&nbsp;
			<p><button class="button--radius">.button--radius</button></p>
			&nbsp;
			<p><button class="button--bordered">.button--bordered</button></p>
		</div>
		<div class="col-12--small col-4--medium">
			<p><button class="button inactive">.button.inactive</button></p>
			&nbsp;
			<p><button class="button--blank inactive">.button--blank.inactive</button></p>
			&nbsp;
			<p><button class="button--bordered inactive">.button--bordered.inactive</button></p>
		</div>
	</div>
</section>
<section class="section--inverted">
	<div class="row">
		<div class="col-12">
			<h1 class="margin-bottom--m">section.section--inverted</h1>
			<h2>Header h2</h2>
			<h3>Header h3</h3>
			<h4>Header h4</h4>
			<h5>Header h5</h5>
			<h6>Header h6</h6>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Paragraphs with quote, strong and em tags.</h2>
		<div class="col-12--small col-6--medium">
			<p>This is content in the <q>formatted wysiwyg section</q> (.section--formatted). It overwrites some of the basic styles, mainly lists. This part is a paragraph with a <strong>strong tag</strong> and some <em>italic content</em>.</p>

			<p>Here is another paragraph inside .formatted--section with another <q>cite</q> and here is a link to <a href="https:google.com" title="Google" alt="Google.com">Google</a>.
				Another cool thing is that we can wrap a word or more text into span with the class <span class="silent">.silent</span> or <span class="loud">.loud</span> to give them some extra visual meening.
			</p>
		</div>
		<div class="col-12--small col-6--medium">
			<blockquote>This is a blockquote</blockquote>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Notifications</h2>
		<div class="col-12--small col-3--medium">
			<p><span class="error">.error gives this color.</span></p>
			<p><span class="warning">.warning gives this color.</span></p>
			<p><span class="success">.success gives this color.</span></p>
		</div>
		<div class="col-12--small col-9--medium">
			<span class="notification-block--error">.notification-block--error</span>
			<span class="notification-block--warning">.notification-block--warning</span>
			<span class="notification-block--success">.notification-block--success</span>
		</div>
	</div>
	<div class="row">
		<h2 class="grid-padding">Buttons</h2>
		<div class="col-12--small col-4--medium">
			<p><button class="button">.button</button></p>
			&nbsp;
			<p><button class="button--blank">.button--blank</button></p>
			&nbsp;
			<p><button class="button--radius">.button--radius</button></p>
			&nbsp;
			<p><button class="button--bordered">.button--bordered</button></p>
		</div>
		<div class="col-12--small col-4--medium">
			<p><button class="button inactive">.button.inactive</button></p>
			&nbsp;
			<p><button class="button--blank inactive">.button--blank.inactive</button></p>
			&nbsp;
			<p><button class="button--bordered inactive">.button--bordered.inactive</button></p>
		</div>
	</div>
</section>
<section class="section--formatted">
	<div class="row">
		<h1 class="margin-bottom--m">section.section--formatted</h1>
		<h2>Lists</h2>
		<ol>
			<li>Ordered list item</li>
			<li>has list-style-type</li>
			<li>only in .section--formatted area.</li>
		</ol>
		<ul>
			<li>Unordered list item</li>
			<li>has list-style-type</li>
			<li>only in .section--formatted area.</li>
		</ul>
	</div>
</section>
<section class="section--inverted">
	<div class="row form-container">
		<form action="" method="POST" class="form col-12--small col-10--medium center--all">				
			<div class="row">
				<div class="input-field">
					<input id="form-name" type="text" name="name" />
					<label for="form-name">Namn</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field">
					<input id="form-subject" type="text" name="subject" />
					<label for="form-subject">Ämne</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field">
					<input id="form-email" type="text" name="email" />
					<label for="form-email">E-post</label>
				</div>
			</div>	
			<div class="row">
				<div class="input-field">
					<textarea type="text" id="form-message" for="message"></textarea>
					<label for="form-message">Fritext</label>
				</div>
			</div>
			<div class="row boxes-container">
				<div class="radioButtons">
					<h3>Nyhetsbrev</h3>
					<input type="radio" id="news-yes" name="news" value="news" checked class="with-gap"/>
					<label for="news-yes">Ja, tack!</label>
					<input type="radio" id="news-no" name="news" value="news" class="with-gap"/>
					<label for="news-no">Nej, tack!</label>
				</div>
				<div class="checkboxes">
					<h3>Följ oss</h3>
					<input type="checkbox" name="social-media" value="social-media" class="filled-in" id="facebook-checkbox"  />
      				<label for="facebook-checkbox">Facebook</label>
					<input type="checkbox" name="social-media" value="social-media" class="filled-in" id="instagram-checkbox"  />
      				<label for="instagram-checkbox">Instagram</label>
					<input type="checkbox" name="social-media" value="social-media" class="filled-in" id="twitter-checkbox"  />
      				<label for="twitter-checkbox">Twitter</label>					  					  
				</div>
			</div>
		</form>
	</div>
</section>
<?php get_footer(); ?>
