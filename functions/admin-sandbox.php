<?php
/**
 * Sandbox admin area for user role editor. Toggle admin nav menu items.
 * @author: Tatsumi Suzuki <tatsumi.suzuki@fastforwardmedia.se>
 * @author: Cecilia Fredriksson
 * @version: 1.0.1
 * @copyright: Copyright (c) 2016, Tatsumi Suzuki, FFMedia AB
 * @link: https://fastforwardmedia.se
 *
 * Table of contents:
 *
 * - Toggle menu items for current user.
 * - Edit "editor" default admin access.
 * - Custom panels for user roles.
 */

$current_user = wp_get_current_user();

if(!isset($current_user)) {
	exit;
}

// Toggle user role specific admin menu items.
if(isset($current_user->roles[0]) && $current_user->roles[0] === 'editor') {

	add_action('admin_menu', 'toggle_admin_menu_items');

	define('USER_ROLE', 'editor');

	// Should we show "posts" or "pages" or "both"
	define('PANELS', 'both');

	add_action('wp_dashboard_setup', 'sandbox_dashboard_widgets');
}

function toggle_admin_menu_items() {

	global $menu;
	global $submenu;

	/**
	 * Add or remove tabs from admin menu when sandboxed user.
	 * Set false/true in 'remove_all' to toggle tab
	 * or set false/true in 'remove_tab' to toggle single sub menu link in the next array.
	 *
	 * Architecture:
	 * 'theme-slug' => ['remove_all' => true/false, 'submenu_slug' => true/false]
	 */
	$remove_menu_tabs = array(
		// Posts
		'edit.php' => array(
			// Toggle all
			'remove_all' => false,
			// All posts
			'edit.php' => false,
			// Add new post
			'post-new.php' => false,
			// Categories
			'edit-tags.php?taxonomy=category' => false,
			// Tags
			'edit-tags.php?taxonomy=post_tag' => false
		),
		// Media
		'upload.php' => array(
			// Toggle all
			'remove_all' => true,
			// Library
			'upload.php' => false,
			// Upload
			'media-new.php' => false
		),
		// Pages
		'edit.php?post_type=page' => array(
			// Toggle all
			'remove_all' => false,
			// All pages
			'edit.php?post_type=page' => false,
			// Add new page
			'post-new.php?post_type=page' => false,
		),
		// Comments
		'edit-comments.php' => array(
			// Toggle all
			'remove_all' => true
		),
		// Appearance
		'themes.php' => array(
			// Toggle all
			'remove_all' => false,
			// Themes
			'themes.php' => true,
			// Theme Editor
			'theme-editor.php' => true
			// TODO: try to add submenu alternatives.
		),
		// Users
		'users.php' => array(
			// Toggle All
			'remove_all' => false
		),
		// Tools
		'tools.php' => array(
			// Toggle All
			'remove_all' => true
		)
	);

	foreach($remove_menu_tabs as $menu_parent => $sub_menu) {

		// Remove parent if 'remove_all' === true
		if($remove_menu_tabs[$menu_parent]['remove_all'] === true) {
			remove_menu_page($menu_parent);
		} else {

			foreach($sub_menu as $menu_child => $remove) {

				// Remove menu child if remove is true.
				if($menu_child !== 'remove_all' && $remove === true) {
					remove_submenu_page($menu_parent, $menu_child);
				}
			}
		}
	}

	// Other tabs, should be turned on but inactivated when needed.
	// remove_submenu_page('index.php','update-core.php');
}

/**
 * Grant access to more stuff for the editor.
 * We will deny access individually from case to case later.
 */
function edit_editor_role() {

	$user_role = get_role('editor');
	$user_role->add_cap('update_core');
	$user_role->add_cap('update_plugins');
	$user_role->add_cap('unfiltered_html');
	$user_role->add_cap('edit_users');
	$user_role->add_cap('create_users');
	$user_role->add_cap('delete_users');
	$user_role->add_cap('remove_users');
	$user_role->add_cap('list_users');
	$user_role->add_cap('edit_theme_options');

	// Special case, removes theme editor from theme tab in admin menu.
	remove_submenu_page( 'themes.php', 'theme-editor.php' );
}

add_action('admin_init', 'edit_editor_role');

/**
 * Show posts/pages or both on admin page.
 * @param $panels string posts|pages or leave empty for both
 */
function sandbox_dashboard_widgets() {

	if(PANELS === 'posts') {
		wp_add_dashboard_widget(
			'post_dashboard_widget',
			'<h2>Inlägg</h2>',
			'post_dashboard_widget_function'
		);
	} elseif(PANELS === 'pages') {

		wp_add_dashboard_widget(
			'page_dashboard_widget',
			'<h2>Sidor</h2>',
			'page_dashboard_widget_function'
		);
	} elseif(PANELS === 'both') {
		wp_add_dashboard_widget(
			'page_dashboard_widget',
			'<h2>Sidor</h2>',
			'page_dashboard_widget_function'
		);

		wp_add_dashboard_widget(
			'post_dashboard_widget',
			'<h2>Inlägg</h2>',
			'post_dashboard_widget_function'
		);
	}

	wp_add_dashboard_widget(
		'custom_post_dashboard_widget',
		'<h2>Nyheter från <a class="ffm-link" href="https://fastforwardmedia.se" title="Fast Forward Media - En personlig mediebyrå" target="_blank">'.
			'<img src="' . get_bloginfo('stylesheet_directory') . '/img/icons/logo-mobile.svg" alt="Information från Fast Forward Media" width="160" /></a></h2>'.
		'<style>a.ffm-link img {display:block;}</style>',
		'custom_post_dashboard_widget_function'
	);

	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
	remove_action('welcome_panel', 'wp_welcome_panel');
}

// Include widget functions for custom panels
function post_dashboard_widget_function() {
	include 'admin/ffm_post_widget.php';
}

function page_dashboard_widget_function() {
	include 'admin/ffm_page_widget.php';
}

function custom_post_dashboard_widget_function() {
	include 'admin/ffm_custom_post_widget.php';
}

/**
 * Remove posts from post widget.
 * @param string $link
 * @param string $before
 * @param string $after
 */
function wp_delete_post_link($link = '', $before = '', $after = '') {
	global $post;

	$link = " | <span class='trash'><a class='submitdelete' href='" . wp_nonce_url(get_bloginfo('url') . "/wp-admin/post.php?action=delete&amp;post=" . $post->ID, 'delete-post_' . $post->ID) . "'>" . $link . "</a></span>";
	echo $before . $link . $after;
}
