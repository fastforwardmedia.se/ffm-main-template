<?php
add_action('after_setup_theme', 'ffm_theme_setup');

function ffm_theme_setup() {
	load_theme_textdomain('ffm_theme', get_template_directory_uri() . '/languages');

	// Crop images for social media.
	add_theme_support('post-thumbnails');

	add_image_size( 'g+_share', 497, 373, true);
	add_image_size( 'twitter_share', 1024, 512, true);
	// Instagram, only for download from server.
	add_image_size( 'instagram_share', 1024, 1024, true);
	add_image_size( 'facebook_share', 1200, 627, true);

	add_post_type_support( 'page', 'excerpt' );

	register_nav_menus(
		array('main-menu' => __('Main Menu', 'ffm_theme'))
	);

	/** Add custom header image **/
	$args = array(
		'width'         => 1920,
		'flex-width'	=> true,
		'height'        => 750,
		'flex-height'    => true,
	);
	add_theme_support( 'custom-header', $args );
}

function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
		'title'       => __( 'Logo', 'themeslug' ),
		'priority'    => 30,
		'description' => 'Upload a logo to replace the default site name and description in the header',
	) );
	$wp_customize->add_setting( 'themeslug_logo' );
	
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'themeslug_logo',
			array(
				'label'      => __( 'Logo', 'themeslug' ),
				'section'    => 'themeslug_logo_section',
				'settings'   => 'themeslug_logo',
			)
		)
	);
	
}

add_action( 'customize_register', 'themeslug_theme_customizer' );

function wp_after_body() {
	do_action('wp_after_body');
}
