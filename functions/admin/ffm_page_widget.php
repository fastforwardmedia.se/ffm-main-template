<div class="wrap">


	<ul>
		<?php
		// Definera WP-QUERY och vilken typ av sidor som ska visas
		$the_query = new WP_Query('post_type=page');

		// Starta WP-query
		while ($the_query->have_posts()) : $the_query->the_post();

			// Visa titel
			?>
			<li><a class="row-title" href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
				<?php
				// Visa redigera knapp, med permalänk till att redigera just de inlägget.
				edit_post_link('Redigera', '<span class="edit">', '</span>');
				?>
			<hr/>
			<?php
		// Loopa igenom alla tills limiten är nått, om man nu vill ha en limit.

		endwhile;

		wp_reset_postdata();
		?>

	</ul>

</div>