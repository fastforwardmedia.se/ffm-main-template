<script type="text/javascript">
var $=jQuery;
(function($) {
	var url = 'http://www.fastforwardmedia.se/customerCampaign-1.0.1.json';

	$.ajax({
	   type: 'GET',
		url: url,
		async: false,
		contentType: "application/json",
		jsonpCallback: 'jsonCallback',
		dataType: 'jsonp'
	})
	.done(function(json) {

		if(typeof json === 'undefined') {
			return;
		}

		var markup = '<div class="ffm-slide">',
			classnames = '';

		for(var i = 0; i < json.campaign.length; i++) {

			if(i !== 0) {
				classnames = ' hidden';
			} else {
				classnames = ' active';
			}

			markup += '<div class="slide-item' + classnames + '">';
			markup += json.campaign[i].header ? '<h2>' + json.campaign[i].header + '</h2>' : '';
			markup += json.campaign[i].img ? '<img src="'+ json.campaign[i].img +'" alt="" width="100%"/>' : '';
			markup += json.campaign[i].body ? '<p>'+json.campaign[i].body+'</p>' : '';
			markup += '</div>';

			classnames = '';
		}
		markup += '</div>';

		$('#custom_post_dashboard_widget').find('.inside').html(markup);
		$(document).trigger('do-ffm-slide');

	});

	$(document).on('do-ffm-slide', function () {

		var $item = $('.slide-item');

		if(typeof $item === 'undefined') {
			return;
		}

		var interval = setInterval(function () {

			if(typeof $item.siblings('.active') === 'undefined') {
				clearInterval(interval);
			}

			if($item.siblings('.active').next().length !== 0) {

				$item
					.siblings('.active')
					.removeClass('active')
					.addClass('hidden')
						.next()
						.addClass('active')
						.removeClass('hidden');
			} else {

				$item
					.siblings('.active')
					.removeClass('active')
					.addClass('hidden');
				$item.first()
					.addClass('active')
					.removeClass('hidden');
			}
		}, 10000);
	});

})(jQuery);
</script>
