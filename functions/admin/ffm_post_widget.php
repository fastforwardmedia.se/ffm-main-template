<style>
	.post_head {
		display: block;
	}
	.post_head a, .post_head p {
		display: inline-block;
	}
	.post_head p {
		margin: 10px;
	}
	.submitdelete {
		color: #a00;
	}
	.submitdelete:hover {
		color: #FF0000;
	}
</style>
<div class="wrap">
	<ul>
<?php
		// Definera WP-QUERY och antal inlägg att visa
		$the_query = new WP_Query('posts_per_page=11');
		
		// Starta WP-query
		$i = 0;
		while ($the_query->have_posts()) {
			$the_query->the_post();
			// Visa titel och datum
?>
			<li>
<?php
				if ($i === 10) {
?>
					<a class="page-title-action" href="<?php echo get_option('siteurl') ?>/wp-admin/edit.php">Visa fler..</a>
<?php
				} else {
?>
					<span class="post_head">
						<a class="row-title" href="<?php the_permalink() ?>"><?php the_title(); ?></a><p><?php echo get_the_date(); ?></p>
					</span>
<?php			
				
					// Visa redigera knapp, med permalänk till att redigera just de inlägget.
					edit_post_link('Redigera', '<span class="edit">', '</span>');
					wp_delete_post_link(' Ta bort', '', '');
?>
					<hr/>
<?php
					
				}
?>
				
			</li>
<?php
			$i++;
		}

		wp_reset_postdata();
?>

	</ul>

</div>