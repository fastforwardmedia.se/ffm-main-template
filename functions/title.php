<?php
add_filter('the_title', 'ffm_theme_title');

function ffm_theme_title($title) {
	if ($title == '') {
		return '&rarr;';
	} else {
		return $title;
	}
}

add_filter('wp_title', 'ffm_theme_filter_wp_title');

function ffm_theme_filter_wp_title($title) {
	return $title . esc_attr(get_bloginfo('name'));
}
