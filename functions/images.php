<?php
/**
 * Functions for image rendering
 * @author: Tatsumi Suzuki <tatsumi.suzuki@fastforwardmedia.se>
 * @version: 1.0.0
 * @copyright: Copyright (c) 2016, Tatsumi Suzuki, FFMedia AB
 * @link: https://fastforwardmedia.se
 *
 * Table of contents:
 *
 * - Base 64 Encode images.
 */

/**
 * Base64 Encode image  and return encoded if possible, otherwise return url to image.
 * @param string $image_path
 * @return string
 */
function base64_encode_image($image_path) {

	$type = pathinfo($image_path, PATHINFO_EXTENSION);

	$data = @file_get_contents($image_path);

	if($data === false) {
		$file = $image_path;
	} else {
		$file = 'data:image/' . $type . ';base64,' . base64_encode($data);
	}

	return $file;
}
