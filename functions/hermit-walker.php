<?php

/*
 * custom walker that only shows the menuitem's ID's (and active items get active classes),
 * delevering clean menu code (in WordPress > 3.0)
*/
class hermit_walker extends Walker_Nav_Menu {
	
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$current_indicators = array('current-menu-item', 'current-menu-parent', 'current_page_item', 'current_page_parent', 'disable', 'mobile-menu-button', 'page-search');

		$newClasses = array();

		foreach($classes as $el){
			//check if it's indicating the current page, otherwise we don't need the class
			if (in_array($el, $current_indicators)){ 
				array_push($newClasses, $el);
			}
		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $newClasses), $item ) );
		
		if($class_names!= '' ) {	
			$class_names = ' class="'. esc_attr( $class_names ) . '"';
		}
		
		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $class_names .'>';
		
		if ($item->title === 'Social') {
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';	
			$attributes .= ! empty( $item->class )      ? ' class="'   . esc_attr( $item->class        ) .'"' : '';
		} else {
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		}
		
		if($depth != 0) {
			//children stuff, maybe you'd like to store the submenu's somewhere?
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id);
	}
}
