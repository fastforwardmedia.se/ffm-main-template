<?php
/**
 * Toggle wich buttons to show in the editor and adjust output without wp-classes.
 *
 * @author: Tatsumi Suzuki <tatsumi.suzuki@fastforwardmedia.se>
 * @version: 1.0.0
 * @copyright: Copyright (c) 2016, Tatsumi Suzuki, FFMedia AB
 * @link: https://fastforwardmedia.se
 *
 * Table of contents
 * - Trim TinyMCE buttons
 * - Toggle image link
 * - Remove width and height from uploaded images in markup.
 */

/**
 * Trim tinyMCE buttons
 * @param $in array TinyMCE buttons
 * @return array
 */
function trim_tinymce_buttons( $in ) {

	$in['paste_remove_styles'] = true;
	$in['paste_remove_spans'] = true;
	$in['paste_strip_class_attributes'] = 'none';
	$in['paste_text_use_dialog'] = true;
	$in['keep_styles'] = true;
	$in['block_formats'] = 'Paragraph=p; Rubrik större=h2; Rubrik mindre=h3';
	$in['toolbar1'] = 'formatselect,bold,italic,underline,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo';

	return $in;
}
add_filter('tiny_mce_before_init', 'trim_tinymce_buttons');

/**
 * Toggle image link allows you to remove a-tag from images in tinyMCE
 */
update_option('image_default_link_type','none');


/**
 * Remove width and height from uploaded images in markup.
 * @param $html
 * @return mixed
 */
function remove_thumbnail_dimensions( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	return $html;
}

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

// Merge and edit this if you like to add classes to markup.
// Remember to add wysiwyg.css to build/css
/*
 function trim_tinymce_buttons( $in ) {
	$in['paste_remove_styles'] = true;
	$in['paste_remove_spans'] = true;
	$in['paste_strip_class_attributes'] = 'none';
	$in['paste_text_use_dialog'] = true;
	$in['keep_styles'] = true;
	$in['wpautop'] = true;
	$in['block_formats'] = 'Stycke=p; Rubrik h1=h1; Rubrik h2=h2; Rubrik h3=h3; Skrivstil stor=h4; Skrivstil mindre=h5';
	$in['toolbar1'] = 'styleselect,formatselect,bold,italic,underline,bullist,numlist,pastetext,hr,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo';
	$in['toolbar2'] = '';

	$style_formats = array(
		array(
			'title' => 'Coustard XL - 64px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h2-coustard',
		),
		array(
			'title' => 'Coustard LG - 52px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h3-coustard',
		),
		array(
			'title' => 'Coustard MD - 42px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h4-coustard',
		),
		array(
			'title' => 'Coustard SM - 30px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h5-coustard',
		),
		array(
			'title' => 'Oswald - 15px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'oswald',
		),
		array(
			'title' => 'Skrivstil stor - 24px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h1-lily',
		),
		array(
			'title' => 'Skrivstil liten - 18px',
			'block' => 'span',
			'wrapper' => true,
			'classes' => 'h2-lily',
		),
	);

	// Insert the array, JSON ENCODED, into 'style_formats'
	$in['style_formats'] = json_encode( $style_formats );

	return $in;
}

add_filter('tiny_mce_before_init', 'trim_tinymce_buttons');

add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {

	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

add_filter( 'mce_css', 'fb_mcekit_editor_style');
*/
/*
function fb_mcekit_editor_style($url) {

	if ( ! empty( $url ) )
		$url .= ',';

	// Retrieves the plugin directory URL
	// Change the path here if using different directories
	$url .= get_template_directory_uri() . '/build/css/wysiwyg.css';

	return $url;
}
*/
