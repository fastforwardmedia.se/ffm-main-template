<?php
add_action('wp_enqueue_scripts', 'ffm_theme_load_scripts');

function ffm_theme_load_scripts() {
	wp_deregister_script('jquery');

	wp_register_script('jquery', '//code.jquery.com/jquery-1.11.3.min.js', false, '1.11.3', true);
	wp_enqueue_script('jquery');

	//wp_register_script('jquery-migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.js', false, '1.2.1', true);
	//wp_enqueue_script('jquery-migrate');

	wp_enqueue_script('scripts', get_template_directory_uri().'/build/js/script.min.js', false, '0.0.1', true);

	// wp_enqueue_script('slick-slider', get_template_directory_uri().'/js/slick-1.5.9/slick/slick.min.js', false, '1.5.9', true);
	// wp_enqueue_script('maps', 'http://maps.google.com/maps/api/js?callback=initMap', false, '0.0.1', true);

	wp_enqueue_style('font-awesome', get_template_directory_uri().'/assets/font-awesome-4.6.3/css/font-awesome.min.css');
	wp_enqueue_style('style', get_template_directory_uri().'/build/css/master.min.css');
}

/**
 * Add defer attribute to enqueued script files so they can be loaded asyncronicly.
 * @param string $tag
 * @param string $handle
 * @return mixed
 */
function ffm_defer_scripts($tag, $handle) {

	// Add script name here if it can be loaded defered.
	switch($handle) {
		case 'jquery-form':
			return str_replace( ' src', ' defer="defer" src', $tag );
		case 'contact-form-7':
			return str_replace( ' src', ' defer="defer" src', $tag );
		default:
			return $tag;

	}
}

add_filter('script_loader_tag', 'ffm_defer_scripts', 10, 2 );
