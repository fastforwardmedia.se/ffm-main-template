<?php get_header(); ?>
<section class="section">
<?php
	if(have_posts()) {
?>
		<header class="row">
			<h1><?php printf( __( 'Sökresultat för: %s', 'ffm_theme' ), get_search_query() ); ?></h1>
		</header>
<?php		
		while(have_posts()) {
			the_post();
?>
			<div class="row">
				<h2><?php the_title();?> <em><?php the_date(); ?></em></h2>
				<a href="<?php the_permalink();?>" title="<?php the_title()?>">Gå till: <?php the_title();?></a>
			</div>
<?php
		}
	} else {
?>
		<div class="row">
			<p>Ingen sida hittades. Använd sökformuläret eller klicka <a href="<?php echo site_url();?>" title="Startsidan">här</a> för att återgå till startsidan.</p>
			<?php get_search_form(); ?>
		</div>
<?php
	}
?>
</section>
<?php 
get_footer();
