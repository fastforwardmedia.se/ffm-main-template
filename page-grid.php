<?php get_header(); ?>
	<section>
		<div class="row">
			<div class="col-12">
				<h2>Rows</h2>
				<p>.row-small, .row, .row-medium, .row-large, .row-full Edit sizes in /assets/scss/_grid-flexbox.scss</p>
			</div>
		</div>
		<div class="demo-columns">
			<div class="row-small">
				<div class="col-12">
					<span>Small row</span>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<span>Default row</span>
				</div>
			</div>
			<div class="row-medium">
				<div class="col-12">
					<span>Medium row</span>
				</div>
			</div>
			<div class="row-large">
				<div class="col-12">
					<span>Large row</span>
				</div>
			</div>
			<div class="row-full">
				<div class="col-12">
					<span>Full row</span>
				</div>
			</div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Columns</h2>
				<p>.col-1 > .col-12</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12"><span></span></div>
		</div>
		<div class="row">
			<div class="col-11"><span></span></div>
		</div>
		<div class="row">
			<div class="col-10"><span></span></div>
		</div>
		<div class="row">
			<div class="col-9"><span></span></div>
		</div>
		<div class="row">
			<div class="col-8"><span></span></div>
		</div>
		<div class="row">
			<div class="col-7"><span></span></div>
		</div>
		<div class="row">
			<div class="col-6"><span></span></div>
		</div>
		<div class="row">
			<div class="col-5"><span></span></div>
		</div>
		<div class="row">
			<div class="col-4"><span></span></div>
		</div>
		<div class="row">
			<div class="col-3"><span></span></div>
		</div>
		<div class="row">
			<div class="col-2"><span></span></div>
		</div>
		<div class="row">
			<div class="col-1"><span></span></div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Offset columns</h2>
				<p>.col-offset-1 > .col-offset-11</p>
			</div>
		</div>
		<div class="row">
			<div class="col-11 col-offset-1"><span></span></div>
		</div>
		<div class="row">
			<div class="col-10 col-offset-2"><span></span></div>
		</div>
		<div class="row">
			<div class="col-9 col-offset-3"><span></span></div>
		</div>
		<div class="row">
			<div class="col-8 col-offset-4"><span></span></div>
		</div>
		<div class="row">
			<div class="col-7 col-offset-5"><span></span></div>
		</div>
		<div class="row">
			<div class="col-6 col-offset-6"><span></span></div>
		</div>
		<div class="row">
			<div class="col-5 col-offset-7"><span></span></div>
		</div>
		<div class="row">
			<div class="col-4 col-offset-8"><span></span></div>
		</div>
		<div class="row">
			<div class="col-3 col-offset-9"><span></span></div>
		</div>
		<div class="row">
			<div class="col-2 col-offset-10"><span></span></div>
		</div>
		<div class="row">
			<div class="col-1 col-offset-11"><span></span></div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Collapsed columns</h2>
				<p>The class .collapse will remove paddings from the element.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6 collapse"><span></span></div>
			<div class="col-6 collapse"><span></span></div>
		</div>
		<div class="row">
			<div class="col-4 collapse"><span></span></div>
			<div class="col-4 collapse"><span></span></div>
			<div class="col-4 collapse"><span></span></div>
		</div>
		<div class="row">
			<div class="col-3 collapse"><span></span></div>
			<div class="col-3 collapse"><span></span></div>
			<div class="col-3 collapse"><span></span></div>
			<div class="col-3 collapse"><span></span></div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Responsive columns</h2>
				<p>Trigger breakpoints xs, sm, md, lg with .col-[method-name]-[number].</p>
				<p>Example: .col-xs-12 .col-sm-6 .col-md-4 .col-lg-3</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><span></span></div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><span></span></div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><span></span></div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><span></span></div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Toggle columns</h2>
				<p>.show-for-[method-name]-[only | up]</p>
				<p>Example: .show-for-xs-only (This will show the content only for extra small browsers).</p>
				<p>Example 2: .show-for-sm-up (This will show the content for small browsers and up).</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 show-for-xs-only"><span>.show-for-xs-only</span></div>
			<div class="col-12 show-for-sm-only"><span>.show-for-sm-only</span></div>
			<div class="col-12 show-for-md-only"><span>.show-for-md-only</span></div>
			<div class="col-12 show-for-lg-only"><span>.show-for-lg-only</span></div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Alignment</h2>
				<p>Example: .start-xs .center-sm .center-md .end-lg</p>
			</div>
		</div>
		<div class="row start-xs center-sm center-md end-lg">
			<div class="show-for-xs-only col-6">
				<span>Start</span>
			</div>
			<div class="show-for-sm-only col-6">
				<span>Center</span>
			</div>
			<div class="show-for-md-only col-6">
				<span>Center</span>
			</div>
			<div class="show-for-lg-up col-6">
				<span>End</span>
			</div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Vertical alignments</h2>
				<p>.top[-size], .middle[-size], .bottom[-size]</p>
				<p>Example: .top</p>
			</div>
		</div>
		<div class="row top">
			<div class="col-6">
				<div class="box">&nbsp;</div>
			</div>
			<div class="col-6">
				<span>Top</span>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>Example: .middle</p>
			</div>
		</div>
		<div class="row middle">
			<div class="col-6">
				<div class="box">&nbsp;</div>
			</div>
			<div class="col-6">
				<span>Middle</span>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>Example: .bottom</p>
			</div>
		</div>
		<div class="row bottom">
			<div class="col-6">
				<div class="box">&nbsp;</div>
			</div>
			<div class="col-6">
				<span>Bottom</span>
			</div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Distribution</h2>
				<p>.around[-size], .between[-size]</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>Example: .around-md</p>
			</div>
		</div>
		<div class="row around-md">
			<div class="col-xs-2">
				<span>
					Around
				</span>
			</div>
			<div class="col-xs-2">
				<span>
					Around
				</span>
			</div>
			<div class="col-xs-2">
				<span>
					Around
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>Example: .between</p>
			</div>
		</div>
		<div class="row between">
			<div class="col-xs-2">
				<span>
					Between
				</span>
			</div>
			<div class="col-xs-2">
				<span>
					Between
				</span>
			</div>
			<div class="col-xs-2">
				<span>
					Between
				</span>
			</div>
		</div>
	</section>
	<section class="demo-columns">
		<div class="row">
			<div class="col-12">
				<h2>Reordering</h2>
				<p>.first[-number]</p>
			</div>
		</div>
		<div class="row">
			<div class="col-2"><span>1</span></div>
			<div class="col-2"><span>2</span></div>
			<div class="col-2"><span>3</span></div>
			<div class="col-2"><span>4</span></div>
			<div class="col-2"><span>5</span></div>
			<div class="col-2 first-xs last-md"><span class="turquoise">6</span></div>
		</div>
	</section>
<?php
get_footer();
