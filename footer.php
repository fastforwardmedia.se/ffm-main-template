	<footer class="section">
		<div class="row">
			<div class="col-12--small col-6--large">
				<?php
				$company_arg = array(
					'post_type' => 'company',
					'posts_per_page' => 1
				);

				$company=  new WP_Query($company_arg);

				while($company->have_posts()) {
					$company->the_post();
					
					$address = get_post_meta(get_the_ID(), 'adress');
					$postnr = get_post_meta(get_the_ID(), 'postnr');
					$tel = get_post_meta(get_the_ID(), 'tel');
					$email = get_post_meta(get_the_ID(), 'email');

					echo '<p>';
						echo $address[0] ? $address[0] : '';
						echo $postnr[0] ? $postnr[0]: '';
					echo '</p>';
					echo '<p>';
					
					echo $tel[0] ? 'Telefon: <a href="tel:'.$tel[0].'" title="Tel"/>'.$tel[0].'</a>' : '';
					echo $email[0] ? '<span>E-mail: <a href="mailto:'.$email[0].'" title="Email"/>'.$email[0].'</a></span>' : '';
					echo '</p>';
					$company_title = get_the_title();
				}
				?>
			</div>
			<div class="col-12--small col-6--large">
				<p>&COPY; <?php echo date('Y') === '2016' ? date('Y') : '2016 - '.date('Y');?> - <?php echo isset($company_title) ? $company_title : ''; ?> - All rights reserved.</p>
				<p class="ffm">Producerad av <a href="https://fastforwardmedia.se" title="www.fastforwardmedia.se"><img src="<?php echo get_template_directory_uri()?>/img/icons/logo-white.svg" alt="Fast Forward Media - En personlig mediebyrå i Stockholm."/></a></p>
			</div>
		</div>
	</footer>
	<script type="text/javascript">
		var templateuri = '<?php echo get_template_directory_uri(); ?>';
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<?php wp_footer(); ?>
    </body>
</html>
