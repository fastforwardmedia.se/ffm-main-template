<?php
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
		?>
		<section class="section section--formatted">
			<div class="row og-content">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</section>
		<?php edit_post_link('Redigera sektion', '<span class="edit-link">', '</span>', get_the_ID()); ?>
<?php
	}
}
get_footer();
