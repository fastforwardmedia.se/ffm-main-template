<?php

// register custom post type for teams.
function create_post_type_event() {

	// define labels for custom post type
	$labels = array(
		'name' => 'Event',
		'singular_name' => 'Event',
		'edit_item' => 'Redigera Event',
		'view_item' => 'Visa Event',
		'not_found' => 'Ingen Event hittad',
		'not_found_in_trash' => 'Ingen Event hittad i papperskorgen',
	);

	$args = array (
		'labels' => $labels,
		'public' => true,
        'supports' => array( 
            'title',   
            'thumbnail'),
        'menu_icon' => 'dashicons-calendar-alt',
		
	);
	register_post_type('Event', $args);
}

add_action('init', 'create_post_type_event');

/**
 * This function creates a metabox to the custom post type
 * @return custom post type with team info
 */
function create_meta_box_event() {
    add_meta_box(
		'event_meta_box_text',
		'Event text:',
		'create_info_metabox_eventtext',
		'Event',
		'normal',
		'default'
	);

    add_meta_box(
		'event_meta_box_date',
		'Eventdate:',
		'create_info_metabox_eventdate',
		'Event',
		'normal',
		'default'
	);
}
add_action('add_meta_boxes', 'create_meta_box_event');

/**
 * This function will add the metabox to the custom post type
 * @return custom post type with team info
 */ 
function event_meta_box_text($post) {
	echo 'Infon skrivs in här';
}
function event_meta_box_date($post) {
	echo 'Infon skrivs in här';
}

/**
 * This function will create the output of the metabox event-date
 * @return input-field in the metabox.
 */ 
function create_info_metabox_eventtext($post) {
		wp_nonce_field('fastforwardmedia_metabox_nonce', 'fastforwardmedia_nonce');
		$content = get_post_meta($post->ID, 'media_insert', true );
		$editor_id = 'media_insert';
		$settings = array(
				'textarea_rows' => 10,
				'media_buttons' => false,
		); 

?>
        <p><strong>Event text</strong></p>
<?php   
		// use add_filter('the_content', 'wpautop');
		// echo wpautop($content[0]); to correctly display and format html for the wp_editor
		wp_editor( $content, $editor_id, $settings );
}

/**
 * This function will create the output of the metabox event-date
 * @return input-field in the metabox.
 */ 
function create_info_metabox_eventdate($post) {
		wp_nonce_field('fastforwardmedia_metabox_nonce', 'fastforwardmedia_nonce');
		$datepicker = get_post_meta($post->ID, 'datepicker', true);
        
?>            
        <form action="" method="post">
            <p>
                <label for="datepicker">Event date</label>
                <input type="text" id="datepicker" name="datepicker" size="26" value="<?php echo esc_attr( $datepicker ); ?>" placeholder="Välj datum" />
            </p>
        </form>
<?php 
}

function save_post_meta($post_id) {
	if(!isset($_POST['fastforwardmedia_nonce']) ||
		!wp_verify_nonce($_POST['fastforwardmedia_nonce'],
		'fastforwardmedia_metabox_nonce')) return;  
	if(isset($_POST['datepicker'])) {
		update_post_meta($post_id, 'datepicker', $_POST['datepicker']);
	}
    if(isset($_POST['media_insert'])) {
		update_post_meta($post_id, 'media_insert', $_POST['media_insert']);
	}

}
add_action('save_post', 'save_post_meta');

// Enqueue Datepicker Script
function enqueue_date_picker() {
    wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('eventcalendar', get_template_directory_uri().'/library/event/event.js');
	wp_enqueue_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);
	// Removes an empty ui-datepicker-div bugg at the bottom of the "add new event"-window in the admin-area
	?><style>#ui-datepicker-div {display: none;}</style><?php

}
add_action( 'admin_enqueue_scripts', 'enqueue_date_picker');
