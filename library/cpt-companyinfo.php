<?php 

// register custom post type for teams.
function create_post_type_company() {
	
	// define labels for custom post type
	$labels = array(
		'name' => 'Företagsinfo',
		'singular_name' => 'Company',
		'edit_item' => 'Redigera Företag',
		'view_item' => 'Visa Företag',
		'not_found' => 'Ingen Företagsinfo hittad',
		'not_found_in_trash' => 'Ingen info hittad i papperskorgen',
	);
	$args = array (
		'labels' => $labels,
		'public' => true,
		'supports' => array(
			'title',
		)
		
	);
	register_post_type('Company', $args);
}
add_action('init', 'create_post_type_company');

/**
 * This function creates a metabox to the custom post type
 * @return custom post type with team info
 */
function create_info_meta_box() {
	add_meta_box(
		'info_metabox',
		'Företagsinfo:',
		'create_info_metabox',
		'Company',
		'normal',
		'default'
	);
}

/**
 * This function will add the metabox to the custom post type
 * @return custom post type with team info
 */ 
function info_metabox($post) {
	echo 'Infon skrivs in här';
}
add_action('add_meta_boxes', 'create_info_meta_box');

/**
 * This function will create the output of the metabox
 * @return input-field in the metabox.
 */ 
function create_info_metabox($post) {
?>
	<form action="" method="post">
<?php
		// add nonce for security
		wp_nonce_field('fastforwardmedia_metabox_nonce', 'fastforwardmedia_nonce');
		// retrive the metadata values if they exist 
		$adress = get_post_meta($post->ID, 'adress', true); 
		$postnr = get_post_meta($post->ID, 'postnr', true); 
		$tel = get_post_meta($post->ID, 'tel', true);
		$email = get_post_meta($post->ID, 'email', true);
		$location = get_post_meta($post->ID, 'location', true);
		
		
		?>
		<p>
			<?php echo 'Adress:' ?> <br/>
			<input type="text" name="adress" value="<?php echo esc_attr( $adress ); ?>" placeholder="företagsgatan 13" />
		</p>
		<p>
			<?php echo 'Postnr:'?> <br/>
			<input type="text" name="postnr" value="<?php echo esc_attr( $postnr ); ?>" placeholder="113 37 Stockholm" />
		</p>
		<p>
			<?php echo 'Tel nr:' ?> <br/>
			<input type="text" name="tel" value="<?php echo esc_attr( $tel ); ?>" placeholder="072-1231231" />
		</p>
		<p>
			<?php echo 'E-mail:' ?> <br/>
			<input type="text" name="email" value="<?php echo esc_attr( $email ); ?>" placeholder="hej@foretag.se" />
		</p>
		<p>
			<?php echo 'Lat/Long:' ?> <br/>
			<input type="text" name="location" value="<?php echo esc_attr( $location ); ?>" placeholder="59.312443,18.0777219" />
		</p>
	</form>
<?php 
}

function save_post_meta($post_id) {
	if(!isset($_POST['fastforwardmedia_nonce']) ||
		!wp_verify_nonce($_POST['fastforwardmedia_nonce'],
		'fastforwardmedia_metabox_nonce')) return;
	
	if(isset($_POST['adress'])) {
		update_post_meta($post_id, 'adress', $_POST['adress']);
	}
	if(isset($_POST['postnr'])) {
		update_post_meta($post_id, 'postnr', $_POST['postnr']);
	}
	if(isset($_POST['tel'])) {
		update_post_meta($post_id, 'tel', $_POST['tel']);
	}
	if(isset($_POST['email'])) {
		update_post_meta($post_id, 'email', $_POST['email']);
	}
	if(isset($_POST['location'])) {
		update_post_meta($post_id, 'location', $_POST['location']);
	}
	
}
add_action('save_post', 'save_post_meta');

