<?php
get_header();
?>
<section class="section">
	<div class="row">
		<h1>404</h1>
		<p>Ingen sida hittades. Använd sökformuläret eller klicka <a href="<?php echo site_url();?>" title="Startsidan">här</a> för att återgå till startsidan.</p>
		<?php get_search_form(); ?>
	</div>
</section>
<?php
get_footer();
