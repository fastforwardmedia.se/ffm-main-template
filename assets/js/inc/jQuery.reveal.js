/**
 * Description: Reveal content when scrolled to.
 * Author: Tatsumi Suzuki
 * Date: 2016-08-12
 * Licence: https://fastforwardmedia.se
 *
 * Example:
 * Add trigger and listen to callback.
 *
 * $(<element>).revealOnScroll({trigger: 'reveal_triggered'});
 *
 * $(document).one('reveal_triggered', function() {
 * 		// Do stuff..
 * });
 */
$.fn.revealOnScroll = function(options) {

	"use_strict";

	var $this = this,
		settings = $.extend({
			trigger: 'undefined'
		}, options);

	function isElementInViewport (el) {

		//special bonus for those using jQuery
		if (typeof jQuery === "function" && el instanceof jQuery) {
			el = el[0];
		}

		var rect = el.getBoundingClientRect();

		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= $(window).height() &&
			rect.right <= $(window).width()
		);
	}

	function onVisibilityChange(el, callback) {
		var old_visible;
		return function () {
			var visible = isElementInViewport(el);

			if(visible != old_visible) {
				old_visible = visible;

				if(typeof callback == 'function' && old_visible == true) {
					callback();
				}
			}
		}
	}

	var handler = onVisibilityChange($this, function() {
		$(document).trigger(settings.trigger);
	});

	$(window).on('DOMContentLoaded load resize scroll', handler);
};
