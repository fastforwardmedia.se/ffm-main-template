/*
 Project: ffm-main-template.
 Description: Main js loaded after all files in assets/inc folder.
 Author: Tatsumi Suzuki
 Date: 2017-02-17
 Licence: https://fastforwardmedia.se
 */

var $ = jQuery;

function FFMjs() {
	this.init();
}

FFMjs.prototype.init = function() {

};

new FFMjs;
