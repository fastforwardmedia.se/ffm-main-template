"use strict";

var lastScrollTopPos = 0,
    scrolledToTop = false,
    scrolledToBottom = false,
    triggerTopPosition = 0,
    triggerBottomPosition = 0;

$(window).on('scroll', function(event){
    hasScrolled();

    if(scrolledToTop === false) {

        if((lastScrollTopPos - 50) <= 0) {
            triggerTopPosition = lastScrollTopPos;
        } 
        else {
            triggerTopPosition = lastScrollTopPos - 50;
        }
        
        scrolledToTop = true;
    }

    if(scrolledToBottom === false) {

        var lastScrollBottomPos = 0;

        if((lastScrollBottomPos + 50) >= 0) {
            triggerBottomPosition = lastScrollBottomPos;
        } 
        else {
            triggerBottomPosition = lastScrollBottomPos + 50;
        }
        
        scrolledToTop = true;
    }
});

function hasScrolled() {

    var currScrollPos = $(this).scrollTop(),
        $header = $('header'),
        $elem = $('footer'),
        headerHeight = $header.outerHeight(),
        bottom_of_window = $(window).scrollTop() + $(window).height(),
        top_of_object = $elem.offset().top;

    if ((currScrollPos < lastScrollTopPos) && (currScrollPos > headerHeight)) {
        $header.addClass('scrolled');

        if(currScrollPos < triggerTopPosition) {
            $header.removeClass('scrolled').addClass('fixed');    
        }
  
    } 
    else {
        $header.removeClass('fixed').removeClass('scrolled');
        triggerTopPosition = currScrollPos - 50;
        scrolledToTop = true;
    }

    if($('footer')[0]) {
     
        if((bottom_of_window <= top_of_object) && (currScrollPos >= lastScrollTopPos)) {

             if(currScrollPos >= triggerBottomPosition) {
                $('.fixed-footer').addClass('fixed');
             }
        } 
        else {
             $('.fixed-footer').removeClass('fixed');
             triggerBottomPosition = currScrollPos + 50;
             scrolledToBottom = true;
        }
    }

    lastScrollTopPos = currScrollPos;
}
