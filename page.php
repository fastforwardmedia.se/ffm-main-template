<?php
get_header();

if(have_posts()) {
?>
<section class="section">
	<?php
	while(have_posts()) {
		the_post();
		?>
		<div class="row og-content">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
			<?php edit_post_link('Redigera sektion', '<span class="edit-link">', '</span>', get_the_ID()); ?>
		</div>
		<?php
	}
	?>
</section>
<?php
}
get_footer();
