<?php
/**
 * Main function file including function parts.
 * @author: Tatsumi Suzuki <tatsumi.suzuki@fastforwardmedia.se>
 * @version: 1.0.2
 * @copyright: Copyright (c) 2016, Tatsumi Suzuki, FFMedia AB
 * @link: https://fastforwardmedia.se
 *
 * Table of contents
 * - Theme setup
 * - Toggle core updates
 * - Theme title setup
 * - Feeds setup
 * - Remove Emojis
 * - Sandbox admin for editors
 * - Hermit walker function for menus
 * - Customize TinyMCE output
 * - Custom Post Types
 */

require 'functions/theme-setup.php';

/**
 * - Core updates
 * - Plugin updates
 * - Theme updates
 * Grant updates: __return_false
 */
add_filter( 'automatic_updater_disabled', '__return_true' );

/**
 * - Load css, js and fonts.
 */
require 'functions/load-scripts.php';

/**
 * - Format theme title
 */
require 'functions/title.php';

/**
 * RSS and other feed related stuff.
 */
require 'functions/feeds.php';

/**
 * Remove emojis
 */
require 'functions/remove-emoji.php';

/**
 * sandbox admin
 */
require 'functions/admin-sandbox.php';

/**
 * Menu walker
 */
require 'functions/hermit-walker.php';

/**
 * Customize TinyMCE output
 */
require 'functions/tinymce-output.php';

/**
 * Image functions
 * - Base64Encode images.
 */
require 'functions/images.php';

/**
 * Custom post types
 */
// Add custom post type "team" and "companyinfo"
require 'library/cpt-companyinfo.php';

// Add custom post type "team" and "event"
//require 'library/event/cpt-event.php';
